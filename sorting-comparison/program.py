import numpy as np
from sorting import pigeonhole, shellsort, merge

# generate random array
length = 10000
unsorted = np.random.randint(1, 100, length)
# print(unsorted)

print("Sorting ", length, " element array ... \n")

pigeonhole.sort(unsorted)

shellsort.sort(unsorted)

merge.sort(unsorted)

