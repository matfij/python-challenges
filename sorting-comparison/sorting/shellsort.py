import time
import numpy as np


def sort(unsorted):

    start_time = time.time()

    length = len(unsorted)
    gap = int(np.floor(length / 2))

    while gap > 0:
        for i in range(gap, length):
            temp = unsorted[i]
            j = i
            while j >= gap and unsorted[j - gap] > temp:
                unsorted[j] = unsorted[j - gap]
                j -= gap

            unsorted[j] = temp
        gap = int(gap/2)

    print("shell sort:", round((time.time() - start_time), 10))
