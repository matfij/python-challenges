import time


def sort(unsorted):

    start_time = time.time()

    # sort array
    minimum = min(unsorted)
    holes = [0] * (max(unsorted) - minimum + 1)

    for number in unsorted:
        holes[number - minimum] += 1

    i = 0
    for cnt in range(len(holes)):
        while holes[cnt] > 0:
            holes[cnt] -= 1
            unsorted[i] = cnt + minimum
            i += 1

    print("pigeonhole:", round((time.time() - start_time), 10))

    # print(unsorted)
