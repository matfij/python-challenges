import time
import numpy as np


def sort(unsorted):

    start_time = time.time()
    sub_sort(unsorted)
    print("merge sort:", round((time.time() - start_time), 10))


def sub_sort(unsorted):

    if len(unsorted) > 1:

        mid = len(unsorted)//2
        left = unsorted[:mid]
        right = unsorted[mid:]

        sub_sort(left)
        sub_sort(right)

        i = j = k = 0

        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                unsorted[k] = left[i]
                i += 1
            else:
                unsorted[k] = right[j]
                j += 1
            k += 1

        while i < len(left):
            unsorted[k] = left[i]
            i += 1
            k += 1

        while j < len(right):
            unsorted[k] = right[j]
            j += 1
            k += 1
