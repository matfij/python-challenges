from django.shortcuts import render
from django.http import HttpResponse


def index(response):
    return HttpResponse("Home")


def gallery(response):
    return HttpResponse("Gallery")
